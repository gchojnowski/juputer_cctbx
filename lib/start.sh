#! /bin/bash


ABSPATH=$(cd "$(dirname "$0")"; pwd)

. $ABSPATH/venvpy3/bin/activate
. $ABSPATH/cctbx_build/setpaths_all.sh

jupyter notebook --kernel=cctbx
